import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FadeInCard extends StatefulWidget {
  final Widget child;
  final Duration duration;

  FadeInCard({this.child, this.duration = const Duration(milliseconds: 500)});

  @override
  _FadeInCardState createState() => _FadeInCardState();
}

class _FadeInCardState extends State<FadeInCard> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _opacityAnimation;
  Animation<double> _rotateAnimation;
  Animation<double> _scaleAnimation;
  Animation<Offset> _slideAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: widget.duration);
    _opacityAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(_controller);
    _rotateAnimation = Tween<double>(begin: 0.0, end: 2 * pi).animate(CurvedAnimation(
        parent: _controller, curve: Interval(0.3, 0.6, curve: Curves.easeInOut)));
    _scaleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller, curve: Interval(0.6, 0.8, curve: Curves.easeInOut)));
    _slideAnimation = Tween<Offset>(begin: Offset(0.0, 0.2), end: Offset.zero).animate(CurvedAnimation(
        parent: _controller, curve: Interval(0.8, 1.0, curve: Curves.easeInOut)));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onTap() {
    _controller.reset();
    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: SlideTransition(
        position: _slideAnimation,
        child: ScaleTransition(
          scale: _scaleAnimation,
          child: RotationTransition(
            turns: _rotateAnimation,
            child: FadeTransition(
              opacity: _opacityAnimation,
              child: Container(
                height: 44,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                ),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: widget.child,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}