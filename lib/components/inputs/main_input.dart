
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class MainInput extends StatelessWidget {
  final Function onChange;
  final String text;
  final Color color;
  final bool obscuring;

  MainInput({this.text, this.onChange, this.color, this.obscuring = false});

  @override
  Widget build(BuildContext context) {
    return TextField(
      style: TextStyle(
        color: Colors.black87,
      ),
        obscuringCharacter: '*',
      obscureText: this.obscuring,
      onChanged: this.onChange,
      decoration: kTextInputDecoration.copyWith(hintText: this.text)
    );
  }
}