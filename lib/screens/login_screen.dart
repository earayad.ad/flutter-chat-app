import 'package:flash_chat/components/buttons/primary_button.dart';
import 'package:flash_chat/constants.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import '../components/inputs/main_input.dart';
import 'chat_screen.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'login_screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  String email = '';
  String password = '';

  void loginByEmailAndPassword (BuildContext context) async {
    final progress = ProgressHUD.of(context);
    try {
      progress?.show();
      final user = await _auth.signInWithEmailAndPassword(email: email, password: password);
      progress?.dismiss();
      if(user != null) {
        Navigator.pushNamed(context, ChatScreen.id);
      }
    } catch(e) {
      print(e);
      progress?.dismiss();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ProgressHUD(
        child: Builder(
        builder: (context) => Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Flexible(
                child: Hero(
                  tag: 'logo',
                  child: Container(
                    height: 200.0,
                    child: Image.asset('images/logo.png'),
                  ),
                ),
              ),
              SizedBox(
                height: 48.0,
              ),
              MainInput(
                text: 'Enter your email.',
                onChange: (value) {
                  this.email = value;
                  //Do something with the user input.
                },
                obscuring: false,
              ),
              SizedBox(
                height: 8.0,
              ),
              MainInput(
                text: 'Enter your password.',
                onChange: (value) {
                  this.password = value;
                  //Do something with the user input.
                },
                obscuring: true,
              ),
              SizedBox(
                height: 24.0,
              ),
              PrimaryButton(
                text: 'Log In',
                color: Colors.lightBlueAccent,
                onPressed: () {
                  loginByEmailAndPassword(context);
                }
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }
}
