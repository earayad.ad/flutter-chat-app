import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flash_chat/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final _firestore = FirebaseFirestore.instance; // TODO: Add firestore instance
final _auth = FirebaseAuth.instance;
ScrollController _scrollController = ScrollController();

class ChatScreen extends StatefulWidget {
  static const String id = 'chat_screen';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController _textEditingController = TextEditingController();
   // TODO: Add firebase auth instance
  String message;

  void sendMessage () {
    if(message == null || message.isEmpty) return;

    try {
      _firestore.collection('messages').add({
        'text': message,
        'sender': _auth.currentUser.email,
        'timestamp': FieldValue.serverTimestamp(),
      });
      _textEditingController.clear();
      _scrollController.animateTo(
        _scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    } catch(e) {
      print(e);
    }

    message = null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                _auth.signOut(); // TODO: Sign out
                Navigator.pop(context);
                //Implement logout functionality
              }),
        ],
        title: Text('⚡️Chat'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            MessageStream(),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: _textEditingController,
                      onChanged: (value) {
                        message = value;
                        //Do something with the user input.
                      },
                      decoration: kMessageTextFieldDecoration,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      sendMessage();
                      //Implement send functionality.
                    },
                    child: Text(
                      'Send',
                      style: kSendButtonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageStream extends StatelessWidget {

@override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firestore.collection('messages').orderBy('timestamp', descending: true).snapshots(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        final messages = snapshot.data.docs;
        // sort messages by timestamp
        List<MessageBubble> messageWidgets = [];
        for(var message in messages) {
          final data = message.data() as Map<String, dynamic>;

          final messageText = data['text'] as String;
          final messageSender = data['sender'] as String;
          final messageTimestamp = data['timestamp'] as Timestamp;
          //final formattedTime = DateForma('HH:mm').format(messageTimestamp.toDate());
          final User currentUser = _auth.currentUser;

          final messageWidget = MessageBubble(
              text: messageText,
              sender: messageSender,
              timestamp: messageTimestamp,
              isMe: currentUser.email == messageSender,
          );
          messageWidgets.add(messageWidget);

        }
        return Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            child: ListView(
              reverse: true,
              controller: _scrollController,
              children: messageWidgets,
            ),
          ),
        );
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String text;
  final String sender;
  final Timestamp timestamp;
  final bool isMe;

  MessageBubble({this.text, this.sender, this.timestamp, this.isMe});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: [
            Text(
              sender,
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.black54,
              ),
            ),
            Material(
              borderRadius: BorderRadius.only(
                topLeft: isMe ? Radius.circular(8.0) : Radius.circular(0.0),
                topRight: isMe ? Radius.circular(0.0) : Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
              color: isMe ? Colors.lightBlueAccent : Colors.green,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        text,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.white,
                        ),
                        // if the text is only one line long, then add a padding to the right to make it look better
                      ),
                      Text(
                        timestamp != null && timestamp.toDate() != null
                            ? // get hour and minuter hh:mm
                            timestamp.toDate().hour.toString().padLeft(2, '0') + ':' + timestamp.toDate().minute.toString().padLeft(2, '0')
                            : '',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10.0)
          ],
        )
    );
  }
}