import 'package:flash_chat/components/buttons/primary_button.dart';
import 'package:flash_chat/components/inputs/main_input.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';


import 'chat_screen.dart';class RegistrationScreen extends StatefulWidget {
  static const String id = 'registration_screen';
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _auth = FirebaseAuth.instance; // TODO: Add firebase auth instance
  String email;
  String password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Hero(
              tag: 'logo',
              child: Container(
                height: 200.0,
                child: Image.asset('images/logo.png'),
              ),
            ),
            SizedBox(
              height: 48.0,
            ),
            MainInput(
              text: 'Enter your email',
              onChange: (value) {
                email = value;
                //Do something with the user input.
              },
            ),
            SizedBox(
              height: 8.0,
            ),
            MainInput(
              text: 'Enter your password',
              onChange: (value) {
                password = value;
                //Do something with the user input.
              },
              obscuring: true,
            ),
            SizedBox(
              height: 8.0,
            ),
            PrimaryButton(
              text: 'Register',
              color: Colors.blueAccent,
              onPressed:  () async {
                try {
                  final newUser = await _auth.createUserWithEmailAndPassword(
                      email: email, password: password);
                  if (newUser != null) {
                    Navigator.pushNamed(context, ChatScreen.id);
                  }
                } catch (e) {
                  print(e);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
